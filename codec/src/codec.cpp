/*
	Experimental code for creating a standalone codec, contributed by IT mines based on existing codebase 
	Work in progress
*/

#include <iostream>
#include <fstream>
#include <vector>

#include <cmath>
#include <pcl/codec/codec.h>


#include <pcl/codec/codec.h>
#include <pcl/codec/impl/codec_impl.hpp>

#include <pcl/quality/quality_metrics.h>
#include <pcl/quality/impl/quality_metrics_impl.hpp>

#include <boost/program_options.hpp>
#include<boost/program_options/parsers.hpp>

#include <assert.h>
#include <sstream>
#include <utility>
#include <pcl/conversions.h>
#include <pcl/PCLPointCloud2.h>

#include <pcl/io/pcd_io.h>
#include <pcl/compression/octree_pointcloud_compression.h>
#include <boost/thread/thread.hpp>

//#warning "This is still work in progress... some variables are not used yet !"

using namespace std;
using namespace pcl;
using namespace pcl::quality;
using namespace pcl::io;
using namespace pcl::octree;
using namespace pcl::console;

namespace po = boost::program_options;
////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**! 
* \struct to store meshes metadata for official evaluation by MPEG committee
* \author Rufael Mekuria (rufael.mekuria@cwi.nl)
*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct codec_mesh_meta_data
{
  string original_file_name;
  size_t original_file_size;
  bool has_coords;
  bool has_normals;
  bool has_colors;
  bool has_texts;
  bool has_conn;
  codec_mesh_meta_data()
    : has_coords(true),  has_normals(false), has_colors(false), has_texts(false), has_conn(false)
  {}
};

//! explicit instantiation of the octree compression modules from pcl
//template class OctreePointCloudCodecV2<PointXYZRGB>;

template class PCL_EXPORTS pcl::io::OctreePointCloudCompression<pcl::PointXYZRGB>;
template class PCL_EXPORTS pcl::io::OctreePointCloudCompression<pcl::PointXYZRGBA>;

typedef OctreePointCloudCompression<PointXYZ> pointsOnlyOctreeCodec;
typedef OctreePointCloudCompression<PointXYZRGB> colorOctreeCodec;
typedef OctreePointCloudCodecV2<PointXYZRGB> colorOctreeCodecV2;
///////////////  Bounding Box Logging /////////////////////////	
// log information on the bounding boxes, which is critical for alligning clouds in time
ofstream bb_out("bounding_box_pre_mesh.txt");
Eigen::Vector4f min_pt_bb;
Eigen::Vector4f max_pt_bb;
bool is_bb_init=false;
double bb_expand_factor = 0.10;
/////////////// END CODEC PARAMETER SETTINGS /////////////////////////

void printHelp (int, char **argv)
	{
	print_error ("Syntax is: %s <mode> < ... >\n\tmode = encode | decode | info\n", argv[0]);
	print_error ("mode encode:\n\t %s encode <input_PLY_file> <output_PCC_file> <octree_bits> <color_bits> <color_encoding_type>\n", argv[0]);
	print_error ("mode decode:\n\t %s encode <input_PCC_file> <output_PLY_file> <octree_bits> <color_bits> <color_encoding_type>\n", argv[0]);
	print_error ("mode encode:\n\t %s info <input_PLY_file>\n", argv[0]);
	}

//! function for loading a mesh file
bool loadPLYMesh (const string &filename, pcl::PolygonMesh &mesh, struct CodecMetadata *codecMetadata )
	{
	TicToc tt;
	print_highlight ("Loading "); 
	print_value ("%s ", filename.c_str ());
	pcl::PLYReader reader;
	tt.tic ();

	if (reader.read (filename, mesh) < 0)
		return (false);

	print_info ("[done, "); 
	print_value ("%g", tt.toc ()); 
	print_info (" ms : "); 
	print_value ("%d", mesh.cloud.width * mesh.cloud.height); 
	print_info (" points]\n");
	print_info ("Available dimensions: "); 
	print_value ("%s\n", pcl::getFieldsList (mesh.cloud).c_str ());

	if ( codecMetadata != NULL )
		{
		codecMetadata->manyPoints = mesh.cloud.width * mesh.cloud.height;
		}

	return (true);
	}

bool loadPLY ( const string &fileName, pcl::PolygonMesh &mesh, struct CodecMetadata *codecMetadata )
	{
	TicToc tt;
	print_highlight ("Loading ");
	print_value ("%s ", fileName.c_str ());
	pcl::PLYReader reader;
	tt.tic ();


	if ( ! boost::filesystem::exists ( fileName ) )
		{
		print_highlight ( "\nFile %s does not exist !\n", fileName.c_str() );
		return false;
		}
	if ( boost::filesystem::is_directory ( fileName ) )
		{
		print_highlight ( "\n %s is a directory, not a file !\n", fileName.c_str() );
		return false;
		}
	if ( ! boost::filesystem::is_regular_file ( fileName ) )
		{
		print_highlight ( "\n %s is not a regular file !\n", fileName.c_str() );
		return false;
		}	

	string fileExtension = boost::filesystem::extension ( fileName );
	if ( fileExtension == ".ply" )
		{
		printf ("\nFile extension ... %s", fileExtension.c_str() );
		loadPLYMesh ( fileName, mesh, codecMetadata );
		// load the metadata 
		codec_mesh_meta_data mdata;
		mdata.original_file_name = fileName;		
		mdata.original_file_size = boost::filesystem::file_size ( fileName );
		
		//! check if the mesh is a point cloud or not
		if ( mesh.polygons.size() > 0 )
			{
			mdata.has_conn = true;
			}
		else
			{		
			mdata.has_conn = false;
			}
		 //! check the fields in the point cloud to detect properties of the mesh
		 #if __cplusplus >= 201103L
		 	for( auto it = mesh.cloud.fields.begin(); it != mesh.cloud.fields.end(); ++it )
		 #else
		 	for( std::vector<pcl::PCLPointField>::iterator it = mesh.cloud.fields.begin(); it != mesh.cloud.fields.end(); ++it )
		 #endif//__cplusplus >= 201103L
		 	{
		 	if( it->name == "rgb")
		 		mdata.has_colors = true;
		 	if( it->name == "normal_x")
		 		mdata.has_colors = true;
		 	if( it->name == "x")
		 		mdata.has_coords = true;
		 	}
		if ( codecMetadata != NULL )
			{
			codecMetadata->hasColors = mdata.has_colors;
			codecMetadata->hasNormals = mdata.has_colors;
			codecMetadata->hasCoords = mdata.has_coords;
			}
		}
	else
		{
		print_highlight ( "\nWe only support " );
		print_value ( "PLY" );
		print_info ( " for now ...\n" );
		}
	return true;
	}


bool encodeData ( pcl::PolygonMesh &mesh, std::stringstream &l_output_base, struct CodecParams *codecParams )
	{
	//std::stringstream l_output_base;
	TicToc tt;
        pcl::quality::QualityMetric achieved_quality;
	uint64_t *c_sizes;

	if ( codecParams == NULL )
		{
		print_highlight ( "\nERROR: Codec parameters were not set !\n" );
		return false;
		}

	colorOctreeCodec::PointCloudPtr l_ptr= colorOctreeCodec::PointCloudPtr ( new colorOctreeCodec::PointCloud() );
	pcl::fromPCLPointCloud2 ( mesh.cloud, *l_ptr );

	#if __cplusplus >= 201103L
        	auto l_codec_encoder = generatePCLOctreeCodecV2<PointXYZRGB> (
	#else
	        boost::shared_ptr<OctreePointCloudCodecV2<PointXYZRGB> > l_codec_encoder = generatePCLOctreeCodecV2<PointXYZRGB> (
	#endif//__cplusplus < 201103L
        		codecParams->octreeBits,
		        codecParams->enhBits,
		        codecParams->colorBits,
		        codecParams->dunno,
		        codecParams->colorCodingType,
		        codecParams->keepCentroid
		        );
	tt.tic ();
	l_codec_encoder->encodePointCloud ( l_ptr ,l_output_base);

        achieved_quality.encoding_time_ms = tt.toc();
	c_sizes = l_codec_encoder->getPerformanceMetrics();
	achieved_quality.byte_count_octree_layer = c_sizes[0];
        achieved_quality.byte_count_centroid_layer = c_sizes[1];
        achieved_quality.byte_count_color_layer= c_sizes[2];

	printf ("\nbyte_count_octree_layer = %ld | byte_count_centroid_layer = %ld | byte_count_color_layer= %ld\n", c_sizes[0], c_sizes[1], c_sizes[2] );

	return true;	
	}

bool encode ( char *inFile, char *outFile, struct CodecParams *codecParams )
	{
	std::stringstream encodedData;
	colorOctreeCodec::PointCloudPtr decodedData;
	ofstream encodedFile;
	pcl::PolygonMesh pointSet;

	loadPLY ( inFile, pointSet, NULL );
	encodeData ( pointSet, encodedData, codecParams );

	// Objects for storing the point clouds.
	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	//pcl::PointCloud<pcl::PointXYZ>::Ptr decompressedCloud(new pcl::PointCloud<pcl::PointXYZ>);

	printf ( "\nDone encoding, writing output file %s ...\n", outFile ); 
	encodedFile.open ( outFile, ios::out | ios::binary ); 
	encodedFile << encodedData.rdbuf();
	encodedFile.close();

	return true;
	}

bool decodeData ( std::stringstream &encodedData, colorOctreeCodec::PointCloudPtr &decoded_cloud_base, struct CodecParams *codecParams )
	{
	TicToc tt;
        pcl::quality::QualityMetric achieved_quality;	
	if ( codecParams == NULL )
                {
                print_highlight ( "\nERROR: Codec parameters were not set !\n" );
                return false;
                }

	#if __cplusplus >= 201103L
	        auto l_codec_decoder_base = generatePCLOctreeCodecV2<PointXYZRGB>(
	#else
        	boost::shared_ptr<OctreePointCloudCodecV2<PointXYZRGB> > l_codec_decoder_base = generatePCLOctreeCodecV2<PointXYZRGB>(
	#endif//__cplusplus >= 201103L
        		codecParams->octreeBits,
		        codecParams->enhBits,
		        codecParams->colorBits,
		        codecParams->dunno,
		        codecParams->colorCodingType,
		        codecParams->keepCentroid
			);

	stringstream oc ( encodedData.str() );
	//colorOctreeCodec::PointCloudPtr decoded_cloud_base = colorOctreeCodec::PointCloudPtr(new colorOctreeCodec::PointCloud);
	decoded_cloud_base = colorOctreeCodec::PointCloudPtr(new colorOctreeCodec::PointCloud);
	tt.tic ();
	l_codec_decoder_base->decodePointCloud ( oc, decoded_cloud_base );

	achieved_quality.decoding_time_ms = tt.toc ();
	
	return true;
	}

bool decode ( char *inFile, char *outFile, struct CodecParams *codecParams )
	{
	std::stringstream encodedData;
	ifstream encodedFile;
	ofstream decodedFile;
	long  beginingIn, endIn;
	
	colorOctreeCodec::PointCloudPtr decodedData;
	pcl::PolygonMesh pointSet;
	
	encodedFile.open ( inFile, ios::in | ios::binary ); 

	encodedFile.seekg (0, ios::beg);
	beginingIn = encodedFile.tellg();
	encodedFile.seekg (0, ios::end);
	endIn = encodedFile.tellg();

	printf ( "\nRead input file %s of size %ld ...\n", inFile, endIn-beginingIn );
	encodedFile.seekg (0, ios::beg);

	encodedData << encodedFile.rdbuf();
	encodedFile.close();
	
	decodeData ( encodedData, decodedData, codecParams );
	pcl::PCLPointCloud2::Ptr cloud2(new pcl::PCLPointCloud2());
        pcl::toPCLPointCloud2( *decodedData, *cloud2 );

	const Eigen::Vector4f origin;
	const Eigen::Quaternionf orientation;
	std::string outFileString ( outFile );

	pcl::PLYWriter writer;

	writer.write ( outFileString, cloud2, origin, orientation, true );

	return true;
	}

bool info ( char *inFile, struct CodecMetadata *codecMetadata )
	{
	pcl::PolygonMesh pointSet;
	loadPLY ( inFile, pointSet, codecMetadata );
	return true;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**! 
* \brief 
*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main (int argc, char** argv)
	{
	vector<string> parameterList;
	string param;
	int paramPos = 0;
	char *inFile;
	char *outFile;
	struct CodecParams *codecParams = NULL; 
	struct CodecMetadata *codecMetadata = NULL; 
	//std::fstream compressedFile;

	if ( argc <= 1 )
  		{
		printHelp ( argc, argv );
		return ( -1 );
		}

	for ( int i = 1; i < argc; i ++ )
		{
		parameterList.push_back ( argv[i] );
		}
	//param = parameterList.pop_back ();
	//
	if ( parameterList [ 0 ] == "encode" || parameterList [ 0 ] == "decode" )
		{
		if ( argc != 7 )
			{
			printHelp ( argc, argv );
			return ( -1 );
			}
		codecParams = ( struct CodecParams *) malloc ( 1 * sizeof ( struct CodecParams ) );
		if ( codecParams == NULL )
			{
			print_highlight ( "\nERROR: Failed to alloc memory for codec params ...\n" );
			return ( -1 );			
			}

		paramPos = paramPos + 1;
		inFile = ( char *) malloc ( MAX_FILE_SIZE * sizeof ( char ) );
		outFile = ( char *) malloc ( MAX_FILE_SIZE * sizeof ( char ) );
		if ( inFile == NULL || outFile == NULL )
			{
			print_highlight ( "\nERROR: Failed to alloc memory for file name ...\n" );
			return ( -1 );
			}

		memset ( inFile, '\0', MAX_FILE_SIZE );
		memset ( outFile, '\0', MAX_FILE_SIZE );

		strcpy ( inFile, parameterList [ paramPos ].c_str() );
		paramPos = paramPos + 1;
		strcpy ( outFile, parameterList [ paramPos ].c_str() );

		paramPos = paramPos + 1;
		codecParams->octreeBits 	= atoi ( parameterList [ paramPos ].c_str() );
		paramPos = paramPos + 1;
		codecParams->colorBits 		= atoi ( parameterList [ paramPos ].c_str() );
		paramPos = paramPos + 1;
		codecParams->colorCodingType 	= atoi ( parameterList [ paramPos ].c_str() );

		if ( parameterList [ 0 ] == "encode" )
			{
			print_highlight ( "\nEncoding %s -> %s ...\n\tOctree Bits = %d\n\tColor Bits = %d\n\tColor Coding Type = %d\n", inFile, outFile, codecParams->octreeBits, codecParams->colorBits, codecParams->colorCodingType );
			encode ( inFile, outFile, codecParams );
			}
		if ( parameterList [ 0 ] == "decode" )
			{
			print_highlight ( "\nDecoding %s -> %s ...\n\tOctree Bits = %d\n\tColor Bits = %d\n\tColor Coding Type = %d\n", inFile, outFile, codecParams->octreeBits, codecParams->colorBits, codecParams->colorCodingType );
			decode ( inFile, outFile, codecParams );
			}
		free ( inFile );
		free ( outFile );
		free ( codecParams );
		}
	else
		{
		if ( parameterList [ 0 ] == "info" )
			{
			print_highlight ( "\nObtaining file info ..." );

			codecMetadata = ( struct CodecMetadata *) malloc ( 1 * sizeof ( struct CodecMetadata ) );
			if ( codecMetadata == NULL )
				{
				print_highlight ( "\nERROR: Failed to alloc memory for codec metadata ...\n" );
				return ( -1 );			
				}

			paramPos = paramPos + 1;
			inFile = ( char *) malloc ( MAX_FILE_SIZE * sizeof ( char ) );
			if ( inFile == NULL )
				{
				print_highlight ( "\nERROR: Failed to alloc memory for file name ...\n" );
				return ( -1 );
				}
			memset ( inFile, '\0', MAX_FILE_SIZE );
			strcpy ( inFile, parameterList [ paramPos ].c_str() );
			info ( inFile, codecMetadata );
			printf ( "\nFileName: %s\n\tPoints: %d\n\tHas Colors: %d\n\tHas Normals: %d\n\tHas Coords: %d\n", inFile, codecMetadata->manyPoints, codecMetadata->hasColors, codecMetadata->hasNormals, codecMetadata->hasCoords );
			free ( inFile );
			free ( codecMetadata );
			}
		else
			{
			print_highlight ( "\nMode not understood... doing nothing ..." );
			}			
		}
	return 0;
	}
