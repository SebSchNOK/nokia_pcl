/*
 * Software License Agreement
 *
 *  Point to plane metric for point cloud distortion measurement
 *  Copyright (c) 2016, MERL
 *
 *  All rights reserved.
 *
 *  Contributors:
 *    Dong Tian <tian@merl.com>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef POINT2PLANE_HPP
#define POINT2PLANE_HPP

#include <pcl/point_types.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>

using namespace std;
using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;
using namespace pcl::search;

namespace pcl {

  namespace point2plane_quality {

    /**!
     * \brief
     *  Store the quality metric for point to plane measurements
     */
    class qMetric {

    public:
      float radius;                //! radius to estimate normals

      // point 2 point ( cloud 2 cloud ), benchmark metric
      float c2c_rms;            //! store symm rms metric
      float c2c_hausdorff;      //! store symm haussdorf

      // point 2 plane ( cloud 2 plane ), proposed metric
      float c2p_rms;            //! store symm rms metric
      float c2p_hausdorff;      //! store symm haussdorf

      qMetric(float radiusIn = 0.3)
      {
        radius = radiusIn;
        c2c_rms = 0; c2c_hausdorff = 0;
        c2p_rms = 0; c2p_hausdorff = 0;
      }
    };

    /**!
     * function to compute the symmetric quality metric: Point 2 Plane
     *   @param cloud_a: point cloud version 1
     *   @param cloud_b: point cloud version 2
     *   @param qual_metric: updated quality metric, to be returned
     * \note
     *   PointT typename of point used in point cloud
     * \author
     *   Dong Tian, MERL
     */
    template<typename PointT> void
      computeP2PlaneQualityMetric(PointCloud<PointT> &cloud_a, PointCloud<PointT> &cloud_b, qMetric &qual_metric)
    {
      cout << "Reporting point cloud distortion metric:\n";
      cout << "  For point 2 point, no normals are used.\n";
      cout << "  For point 2 plane, normal estimation is in use.\n\n";

      // Use "a" as reference
      qMetric metricA(qual_metric.radius);
      findMetric( cloud_a, cloud_b, metricA );

      cout << "1. Use infile1 (A) as reference on which normals are estimated (B->A).\n";
      cout << "   rms       (p2point): " << metricA.c2c_rms << endl;
      cout << "   hausdorff (p2point): " << metricA.c2c_hausdorff << endl;
      cout << "   rms       (p2plane): " << metricA.c2p_rms << endl;
      cout << "   hausdorff (p2plane): " << metricA.c2p_hausdorff << endl;

      // Use "b" as reference
      qMetric metricB(qual_metric.radius);
      findMetric( cloud_b, cloud_a, metricB );

      cout << "2. Use infile2 (B) as reference on which normals are estimated (A->B).\n";
      cout << "   rms       (p2point): " << metricB.c2c_rms << endl;
      cout << "   hausdorff (p2point): " << metricB.c2c_hausdorff << endl;
      cout << "   rms       (p2plane): " << metricB.c2p_rms << endl;
      cout << "   hausdorff (p2plane): " << metricB.c2p_hausdorff << endl;

      // Derive the final symmetric metric
      qual_metric.c2c_rms = max( metricA.c2c_rms, metricB.c2c_rms );
      qual_metric.c2c_hausdorff = max( metricA.c2c_hausdorff, metricB.c2c_hausdorff	);
      qual_metric.c2p_rms = max( metricA.c2p_rms, metricB.c2p_rms );
      qual_metric.c2p_hausdorff = max( metricA.c2p_hausdorff, metricB.c2p_hausdorff );

      cout << "3. Final (symmetric).\n";
      cout << "   rms       (p2point): " << qual_metric.c2c_rms << endl;
      cout << "   hausdorff (p2point): " << qual_metric.c2c_hausdorff << endl;
      cout << "   rms       (p2plane): " << qual_metric.c2p_rms << endl;
      cout << "   hausdorff (p2plane): " << qual_metric.c2p_hausdorff << endl;
    }

    /**!
     * function to compute "one-way" quality metric: Point 2 Plane
     *   @param cloud_a: Reference point cloud. e.g. the original cloud, on which normals would be estimated
     *   @param cloud_b: Processed point cloud. e.g. the decoded cloud
     *   @param metric: updated quality metric, to be returned
     * \note
     *   PointT typename of point used in point cloud
     * \author
     *   Dong Tian, MERL
     */
    template<typename PointT> void
      findMetric(PointCloud<PointT> &cloud_a, PointCloud<PointT> &cloud_b, qMetric &metric)
    {
      // Step 1 ------------------
      // @DT: Compute the normals of A, the reference point cloud
      // Create the normal estimation class, and pass the input dataset to it
      NormalEstimation<PointXYZ, Normal> ne;
      ne.setInputCloud (cloud_a.makeShared());

      // Create an empty kdtree representation, and pass it to the normal estimation object.
      // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
      search::KdTree<PointXYZ>::Ptr tree (new search::KdTree<PointXYZ>());
      ne.setSearchMethod(tree);

      // Output datasets
      PointCloud<Normal>::Ptr cloud_normals (new PointCloud<Normal>);

      // Use all neighbors in a sphere of radius 3cm
      ne.setRadiusSearch(metric.radius);  // 0.03

      // Compute the features
      ne.compute(*cloud_normals);

      // Check if any nan normals
      size_t nanNormal = 0;
      for (size_t i = 0; i < cloud_a.points.size(); i++)
      {
        if ( isnan(cloud_normals->at(i).normal_x) || isnan(cloud_normals->at(i).normal_y) || isnan(cloud_normals->at(i).normal_z) )
          nanNormal++;
      }
      if (nanNormal > 0)
      {
        cerr << "** Warning: nan normals found: " << nanNormal << "! Increase the radius!" << endl;
        // cout << "The points with nan normals would be excluded from metric calculation." << endl;
      }

      // Step 2 ------------------
      // @DT: Find the nearest neighbor point in cloud_a for each point in cloud_b
      //      Set the error vector, from the point in cloud_a to the point in cloud_b
      float max_dist_b = -std::numeric_limits<float>::max();
      double rms_dist_b = 0;
      float distTmp = 0;
      search::KdTree<PointT> tree_a;
      tree_a.setInputCloud (cloud_a.makeShared());

      // A indices buffer, storing the index in cloud_a, that is the NN in cloud_b
      std::vector<int> indicesInA( cloud_b.points.size() );
      PointCloud<PointXYZ> errVector;
      errVector.reserve( cloud_b.size() );

      for (size_t i = 0; i < cloud_b.points.size(); ++i)
      {
        std::vector<int> indices(1);
        std::vector<float> sqr_distances(1);

        tree_a.nearestKSearch(cloud_b.points[i], 1, indices, sqr_distances);
        indicesInA[i] = indices[0];

        errVector.points[i].x = cloud_a.points[indices[0]].x - cloud_b.points[i].x;
        errVector.points[i].y = cloud_a.points[indices[0]].y - cloud_b.points[i].y;
        errVector.points[i].z = cloud_a.points[indices[0]].z - cloud_b.points[i].z;

        distTmp = sqrt( errVector.points[i].x * errVector.points[i].x + errVector.points[i].y * errVector.points[i].y + errVector.points[i].z * errVector.points[i].z );
        rms_dist_b += distTmp;
        if (distTmp > max_dist_b)
          max_dist_b = distTmp;
      }
      rms_dist_b = rms_dist_b / cloud_b.points.size();
      
      metric.c2c_rms = rms_dist_b;
      metric.c2c_hausdorff = max_dist_b;

      // Step 3 -------------------
      // @DT: Compute the projected distance along the normal direction (cloud 2 plane)
      max_dist_b = -std::numeric_limits<float>::max();
      rms_dist_b = 0;

      std::vector<float> distProj( cloud_b.points.size() );
      size_t num = 0;
      for (size_t i = 0; i < cloud_b.points.size(); ++i)
      {
        if ( !isnan(cloud_normals->at(indicesInA[i]).normal_x) && !isnan(cloud_normals->at(indicesInA[i]).normal_y) && !isnan(cloud_normals->at(indicesInA[i]).normal_z) )
        {
          distProj[i] = fabs( errVector.points[i].x * cloud_normals->at( indicesInA[i] ).normal_x +
                              errVector.points[i].y * cloud_normals->at( indicesInA[i] ).normal_y +
                              errVector.points[i].z * cloud_normals->at( indicesInA[i] ).normal_z );
          num++;

          // mean square distance
          rms_dist_b += distProj[i];
          if (distProj[i] > max_dist_b)
            max_dist_b = distProj[i];
        }
      }
      rms_dist_b = rms_dist_b / num; // cloud_b.points.size()

      metric.c2p_rms = rms_dist_b;
      metric.c2p_hausdorff = max_dist_b;
    }

  };
  
}   //~ namespace pcl

#endif
